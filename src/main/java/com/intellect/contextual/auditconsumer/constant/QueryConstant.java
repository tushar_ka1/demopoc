package com.intellect.contextual.auditconsumer.constant;

/** The type Constant. */

public class QueryConstant {

	/** The constant tableName. */
	public static final String ICP_UPSTREAM_AUDIT = "ICP_UPSTREAM_AUDIT";
	public static final String ICP_RECOMMENDATION_AUDIT = "ICP_RECOMMENDATION_AUDIT";
	public static final String ICP_PROCESSING_AUDIT = "ICP_PROCESSING_AUDIT";
	public static final String ICP_EXCEPTION_LOG = "ICP_EXCEPTION_LOG";

	/** The constant ICP_UPSTREAM_AUDIT.ColumnName. */
	public static final String API_NAME = "API_NAME";
	public static final String API_CHANNEL_REQUEST_ID = "API_CHANNEL_REQUEST_ID";
	public static final String API_CP_REFERENCE_ID = "API_CP_REFERENCE_ID";
	public static final String API_ORIG_CHANNEL_REQUEST_ID = "API_ORIG_CHANNEL_REQUEST_ID";
	public static final String API_DATE_TIME = "API_DATE_TIME";
	public static final String API_SYSTEM_CODE = "API_SYSTEM_CODE";
	public static final String API_REQ_RESP = "API_REQ_RESP";
	public static final String API_URI = "API_URI";
	public static final String API_BODY = "API_BODY";
	public static final String API_HEADER = "API_HEADER";
	public static final String API_LAYER = "API_LAYER";
	public static final String API_STAGE = "API_STAGE";
	public static final String API_FINE_GRAIN = "API_FINE_GRAIN";
	public static final String API_OFFERING_CODE = "API_OFFERING_CODE";
	public static final String API_DECISION_PARAM = "API_DECISION_PARAM";
	public static final String API_FIELD_CODE = "API_FIELD_CODE";
	public static final String API_VRULE_CODE = "API_VRULE_CODE";
	public static final String API_ERULE_CODE = "API_ERULE_CODE";
	public static final String API_PAYLOAD = "API_PAYLOAD";
	public static final String API_STATUS = "API_STATUS";
	public static final String ALU_INSERTED_TIME = "ALU_INSERTED_TIME";

	/** The constant ICP_RECOMMENDATION_AUDIT.ColumnName. */
	public static final String API_GR_CP_REFERENCE_ID = "API_GR_CP_REFERENCE_ID";
	public static final String API_GR_CHANNEL_REQUEST_ID = "API_GR_CHANNEL_REQUEST_ID";
	public static final String API_CP_RECOMMENDATION = "API_CP_RECOMMENDATION";
	public static final String API_USER_SELECTION = "API_USER_SELECTION";
	public static final String API_SR_CHANNEL_REQUEST_ID = "API_SR_CHANNEL_REQUEST_ID";

	/** The constant ICP_EXCEPTION_LOG.ColumnName. */

	public static final String API_ERROR_TRACE = "API_ERROR_TRACE";
	public static final String API_ERROR_MSG = "API_ERROR_MSG";

	/** The constant. */
	public static final String GET_RECOMMENDATION = "Get Recommendation";
	public static final String OFFERING_CODE = "offeringCode";
	public static final String REQUEST_ID = "requestId";
	public static final String ORIGINAL_CHANNEL_REQUEST_ID = "originalChannelRequestId";
	public static final String RES = "RES";

	public static final String API_VALUE = "API_VALUE";
}
