/**
 * These materials are confidential and proprietary to Intellect Design Arena Ltd. and no part of
 * these materials should be reproduced, published, transmitted or distributed in any form or by any
 * means, electronic, mechanical, photocopying, recording or otherwise, or stored in any information
 * storage or retrieval system of any nature nor should the materials be disclosed to third parties
 * or used in any other manner for which this is not authorized, without the prior express written
 * authorization of Intellect Design Arena Ltd.
 *
 * <p>Copyright 2019 Intellect Design Arena Limited. All rights reserved.
 */
/**
 * ******************************************************************* Version Control Block
 *
 * <p>Date Version Author Description --------- -------- --------------- ---------------------------
 * 06-08-2019 1.0 YF1 *******************************************************************
 */
package com.intellect.contextual.auditconsumer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/** The type Audit context. */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuditContext {
  Object cpReferenceId;
  String channelRequestId;
  Date dateTime;
  String layer;
  String stage;
  String fineGrain;
  String apiValue;
  String offeringCode;
  String decisionParam;
  String fieldCode;
  String vRuleCode;
  String eRuleCode;
  String payload;
  String status;
  Date aluInsertedTime;
  String apiType;
  String apiUri;
  String srchannelRequestId;
  String cpOffCode;
  String userSelCode;
  String reqRes;
  String systemCode;
  String orginalRequestId;
  String errorMsg;
  String errorTrace;
}
