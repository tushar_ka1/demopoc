/**
 * These materials are confidential and proprietary to Intellect Design Arena Ltd. and no part of
 * these materials should be reproduced, published, transmitted or distributed in any form or by any
 * means, electronic, mechanical, photocopying, recording or otherwise, or stored in any information
 * storage or retrieval system of any nature nor should the materials be disclosed to third parties
 * or used in any other manner for which this is not authorized, without the prior express written
 * authorization of Intellect Design Arena Ltd.
 *
 * <p>Copyright 2019 Intellect Design Arena Limited. All rights reserved.
 */
/**
 * ******************************************************************* Version Control Block
 *
 * <p>Date Version Author Description --------- -------- --------------- ---------------------------
 * 02-Aug-2019 1.0 Ashish Powar *******************************************************************
 */
package com.intellect.contextual.auditconsumer.dao;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.String.format;

/** The type Cp audit dao. */
@Repository
@Slf4j
public class ContextualAuditDaoImpl implements ContextualAuditDao {
  @Autowired DataSourceConfiguration CpDataSrc;
  @Autowired private JdbcTemplate jdbcTemplate;

  @Transactional
  public int insert(@NonNull String tblName, @NonNull Map<String, Object> columnValueMap) {
    List<String> col = new ArrayList<>();
    List<Object> values = new ArrayList<>();
    String query;
    String wildChar;
    String columnList;
    int result = 0;

    for (Map.Entry<String, Object> entry : columnValueMap.entrySet()) {
      col.add(entry.getKey());
      values.add(entry.getValue());
    }

    wildChar =
        IntStream.range(0, col.size()).boxed().map(i -> "?").collect(Collectors.joining(","));
    columnList = String.join(",", col);

    query = format("INSERT INTO %s (%s) VALUES (%s)", tblName, columnList, wildChar);
    Object[] val = values.toArray(new Object[0]);
    result = jdbcTemplate.update(query, val);
    return result;
  }

  @Transactional
  public int update(
      @NonNull String tblName,
      @NonNull Map<String, Object> columnValueMap,
      @NonNull Map<String, Object> whereColumnValueMap) {
    String query;
    String setClause;
    String whereClause;
    List<Object> valueList = new ArrayList<>();

    Function<Map<String, Object>, String> setClauseMaker =
        map ->
            map.entrySet().stream()
                .map(
                    entry -> {
                      valueList.add(entry.getValue());
                      return format("%s = ?", entry.getKey());
                    })
                .collect(Collectors.joining(","));

    Function<Map<String, Object>, String> whereClauseMaker =
        map ->
            map.entrySet().stream()
                .map(
                    entry -> {
                      valueList.add(entry.getValue());
                      return format("%s = ?", entry.getKey());
                    })
                .collect(Collectors.joining(" and "));

    setClause = setClauseMaker.apply(columnValueMap);
    whereClause = whereClauseMaker.apply(whereColumnValueMap);

    log.info("set : {}\nwhere : {}", setClause, whereClause);
    Object[] val = valueList.toArray(new Object[0]);
    query = String.format("UPDATE %s SET %s WHERE %s", tblName, setClause, whereClause);
    return jdbcTemplate.update(query, val);
  }

  public List<Map<String, Object>> qryResult(
      String strdataSource, String qry, List<Object> values) {
    Object[] val = values.toArray(new Object[0]);
    return CpDataSrc.getJDBCTemplate(strdataSource).queryForList(qry, val);
  }
}
