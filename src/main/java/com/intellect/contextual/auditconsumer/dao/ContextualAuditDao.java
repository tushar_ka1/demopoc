/**
 * These materials are confidential and proprietary to Intellect Design Arena Ltd. and no part of
 * these materials should be reproduced, published, transmitted or distributed in any form or by any
 * means, electronic, mechanical, photocopying, recording or otherwise, or stored in any information
 * storage or retrieval system of any nature nor should the materials be disclosed to third parties
 * or used in any other manner for which this is not authorized, without the prior express written
 * authorization of Intellect Design Arena Ltd.
 *
 * <p>Copyright 2019 Intellect Design Arena Limited. All rights reserved.
 */
/**
 * ******************************************************************* Version Control Block
 *
 * <p>Date Version Author Description --------- -------- --------------- ---------------------------
 * 02-Aug-2019 1.0 Ashish Powar *******************************************************************
 */
package com.intellect.contextual.auditconsumer.dao;

import java.util.Map;

/** The interface Cp audit dao. */
public interface ContextualAuditDao {
  /**
   * Insert int.
   *
   * @param tblName the tbl name
   * @param columnValueMap the column value map
   * @return the int
   */
  int insert(String tblName, Map<String, Object> columnValueMap);

  /**
   * Update int.
   *
   * @param tblName the tbl name
   * @param columnValueMap the column value map
   * @param whereColumnValueMap the where column value map
   * @return the int
   */
  int update(
      String tblName, Map<String, Object> columnValueMap, Map<String, Object> whereColumnValueMap);
}
