package com.intellect.contextual.auditconsumer.queuelistener;

import com.intellect.contextual.auditconsumer.dao.AuditProcessing;
import com.intellect.contextual.auditconsumer.model.AuditContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/** @author vicky.parmar Rabbit MQ class listener. */
@Component
@Slf4j
public class QueueListener {

  @Autowired private AuditProcessing upStreamAudit;

  @RabbitListener(queues = "${cpAudit.queue.name}")
  public void receiveMessage(AuditContext auditContext) {
    upStreamAudit.saveAuditInternal(auditContext);
  }

  @RabbitListener(queues = "${cpAudit.upstream.queue.name}")
  public void upStreamAudit(AuditContext auditContext) {
    upStreamAudit.saveUpstreamAudit(auditContext);
  }

  @RabbitListener(queues = "${cpAudit.recommendation.queue.name}")
  public void reccomStreamAudit(String requestBody) {
    upStreamAudit.saveRecommendationAudit(requestBody);
  }

  /*@RabbitListener(queues = "${cpAudit.downstream.queue.name}")
  public void downStreamAudit(AuditContext auditContext) {
  	upStreamAudit.saveAuditInternal(auditContext);
  }
  */

}
